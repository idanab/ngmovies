import {Injectable} from "@angular/core";
import {MoviesService} from "./movies.service";

@Injectable()
export class MoviesBl{

    movies:any[];

    constructor(private proxy:MoviesService){}

    getPopular(){

        let sub = this.proxy
            .getPopular()
            .map(data=>data.results)
            .subscribe(movies=>{
                this.movies = movies;
                sub.unsubscribe();
            });
    }
}