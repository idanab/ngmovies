/**
 * Created by Eyal on 29/01/2017.
 */
import {NgModule} from "@angular/core";
import {MoviesBl} from "./moivesBl.service";
import {MoviesService} from "./movies.service";
import {JsonpModule} from "@angular/http";

@NgModule({
    providers   :[
      MoviesService,
      MoviesBl
    ],

    imports     :[
        JsonpModule
    ],
    exports     :[]
})
export class CoreModule{}