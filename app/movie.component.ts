/**
 * Created by owner on 29/01/2017.
 */
import {Component, Input} from "@angular/core";

@Component({
  selector: 'movie', 
  styles : [`
    .example-card {
        width: 400px;
    }
    
    .example-header-image {
        background-size: cover;
    }
  `],
  template: `
<div>
   <md-card class="example-card">
          <md-card-header>
            <div md-card-avatar class="example-header-image"></div>
            <md-card-title>{{details.original_title}}</md-card-title>
          </md-card-header>
          <img md-card-image src="https://image.tmdb.org/t/p/w300/{{details.backdrop_path}}">
          <md-card-content>
            <p>
              {{details.overview}}
            </p>
          </md-card-content>
    </md-card>
 
 
</div>
`})
export class MovieComponent {
    @Input("source") details:any[];

}