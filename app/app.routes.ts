/**
 * Created by owner on 29/01/2017.
 */
import {Route} from "@angular/router";
import {PopularComponent} from "./popular/popular.component";
import {NewComponent} from "./new/new.component";

export const appRoutes:Route[] =[
    { path: '', pathMatch:'full', redirectTo: 'popular'},
    { path: 'popular', component: PopularComponent},
    { path: 'new', component: NewComponent},
    { path: '**', redirectTo: 'new'}
];