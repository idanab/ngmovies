/**
 * Created by owner on 29/01/2017.
 */
import {Component} from "@angular/core";
import {MoviesBl} from "../services/moivesBl.service";

@Component({
  selector: 'popular',
  styles : [],
  template: `
<div>
    <md-grid-list cols="3" rowHeight="100px">
      <md-grid-tile
          *ngFor="let m of movies"
          [colspan]="1"
          [rowspan]="5">
        <movie [source]="m"></movie>
      </md-grid-tile>
    </md-grid-list>
  <!--<div *ngFor="let m of movies">{{m | json}}</div>-->
</div>
`})
export class PopularComponent {
    constructor(private bl:MoviesBl){
        bl.getPopular();
    }

    get movies(){
        return this.bl.movies;
    }

}