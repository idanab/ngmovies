import {NgModule} from "@angular/core";
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {ComponentsModule} from "./components/components.module";
import {CoreModule} from "./services/core.module";
import {MovieComponent} from "./movie.component";
import {MaterialModule} from "@angular/material";
import {PopularComponent} from "./popular/popular.component";
import {RouterModule} from "@angular/router";
import {appRoutes} from "./app.routes";
import {NewComponent} from "./new/new.component";

@NgModule({
    declarations:[
        AppComponent,
        MovieComponent,
        PopularComponent,
        NewComponent
    ],
    providers   :[],
    bootstrap   :[AppComponent],
    imports     :[
        BrowserModule,
        ComponentsModule,
        RouterModule.forRoot(appRoutes,{
            useHash : true
        }),
        CoreModule,
        MaterialModule.forRoot()
    ],
    exports     :[]
})
export class AppModule{}
