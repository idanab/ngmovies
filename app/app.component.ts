/**
 * Created by Eyal on 29/01/2017.
 */
import {Component} from "@angular/core";

@Component({
  selector: 'myApp',
  styles : [],
  template: `
<div>
  <h1>Movies App</h1>
    <nav>
        <a routerLink="/popular" routerLinkActive="active">Popular</a> |
        <a routerLink="/new" routerLinkActive="active">New</a> |

        <!--<a routerLink="/home" routerLinkActive="active">????</a> |-->
    </nav>
    <div class="box">
        <router-outlet></router-outlet>
    </div>
</div>
`})
export class AppComponent {

}